# Assignment #3 - Prolog/Java/Scala #

This repo contains the solution of the third assignment for the course "Paradigmi di Programmazione e Sviluppo" (PPS) running at Ingegneria e Scienze Informatiche, a.a. 2016/2017 - University of Bologna.

In this third assignment, we were asked to put into practice what we have learnt about Prolog and Java-Prolog(/Scala) integration.
The goal was to produce a new application or an extension/modification of the provided TTT one.

### Number Scrabble ###
We create a new GUI-based application to make two players play the [Number Scrabble] game.
[Number Scrabble]:https://en.wikipedia.org/wiki/Number_Scrabble
All the "model" aspects have been made with [tuProlog], while have been used Java and Scala to build GUI and Scala-Prolog bridge.
[tuProlog]:http://apice.unibo.it/xwiki/bin/view/Tuprolog/WebHome

### Developers ###
* [Ashley Caselli](https://bitbucket.org/ashleycaselli)
* [Francesca Tassinari](https://bitbucket.org/Francesca12/)