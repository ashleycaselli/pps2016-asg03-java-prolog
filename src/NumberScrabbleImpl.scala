import java.io.FileInputStream
import java.util
import java.util.Optional

import Predicate._
import Scala2P._
import alice.tuprolog.{Struct, Theory}

import scala.collection.JavaConverters._
import scala.collection.mutable.Buffer


/**
  * Created by ashleycaselli on 28/04/17.
  */
class NumberScrabbleImpl(fileName: String) extends NumberScrabble {

    private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
    createBoard

    implicit private def playerToString(player: Player): String = player match {
        case Player.PlayerX => "p1"
        case _ => "p2"
    }

    implicit private def stringToPlayer(s: String): Player = s match {
        case "p1" => Player.PlayerX
        case _ => Player.PlayerO
    }

    implicit private def stringToCell(s: String): Cell = {
        val w = s filterNot ("{" contains _) filterNot ("}" contains _)
        val value = w split (",") toList 0
        val player = w split (",") toList 1
        val pl = solveOneAndGetTerm(engine, symbol(s"P, ${player}"), "P").toString
        CellImpl(pl, value.toInt)
    }

    override def createBoard: Unit = {
        val goal = s"${retractall(board("_"))}, ${Predicate.createBoard("B")}, ${assert(board("B"))}"
        solveWithSuccess(engine, goal)
    }

    override def getBoard: util.List[Optional[Cell]] = {
        val term = solveOneAndGetTerm(engine, board("B"), "B").asInstanceOf[Struct]
        val iterator = term.listIterator()
        iterator.asScala.toList.map(_.toString).map {
            case "{null,null}" => Optional.empty[Cell]()
            case s => Optional.of[Cell](s)
        }.to[Buffer].asJava
    }

    override def move(player: Player, value: Int, i: Int, j: Int): Boolean = {
        val goal1 = symbol(s"${playerToString(player)}, P")
        val p = solveOneAndGetTerm(engine, goal1, "P").toString
        val goal2 = s"${board("B")}, ${insertAt(s"B, ${value}, ${p}, ${i * NumberScrabbleApp.BOARD_ORDER + j}, B2")}, ${retractall(board("_"))}, ${assert(board("B2"))}"
        solveWithSuccess(engine, goal2)
    }

    override def changeTurn(currentPlayer: Player): Player = {
        val goal = otherPlayer(s"${playerToString(currentPlayer)}, P")
        solveOneAndGetTerm(engine, goal, "P").toString
    }

    override def getBoardOrder: Optional[Integer] = {
        Optional.of(solveOneAndGetTerm(engine, boardOrder("X"), "X").asInstanceOf[alice.tuprolog.Int].intValue())
    }

    override def checkVictory(player: Player): Boolean = {
        val goal = s"${board("B")}, ${symbol(s"${playerToString(player)}, P")}, ${Predicate.checkVictory("B, P")}"
        solveWithSuccess(engine, goal)
    }

    override def getResult(player: Player): String = {
        val goal = result(s"${playerToString(player)}, R")
        solveOneAndGetTerm(engine, goal, "R").toString
    }
}
