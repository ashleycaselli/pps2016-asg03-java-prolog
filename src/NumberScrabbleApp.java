import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/**
 * Created by ashleycaselli on 28/04/17.
 */
public class NumberScrabbleApp {

    public static int BOARD_ORDER;
    private final Dimension FRAME_DIMENSION = new Dimension(300, 330);

    private final NumberScrabble ns;
    private final JTextField[][] board;
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("Number Scrabble");
    private boolean finished = false;
    private Player currentPlayer;
    private int moves = 0;

    public NumberScrabbleApp(final NumberScrabble ns) {
        this.ns = ns;
        setBoardOrder();
        this.board = new JTextField[BOARD_ORDER][BOARD_ORDER];
        initPane();
    }

    private void setBoardOrder() {
        Optional<Integer> o = this.ns.getBoardOrder();
        if (o.isPresent()) {
            BOARD_ORDER = o.get();
        } else {
            throw new RuntimeException("Error loading board dimension");
        }
    }

    private void initPane() {
        this.frame.setLayout(new BorderLayout());
        JPanel b = new JPanel(new GridLayout(BOARD_ORDER, BOARD_ORDER));
        for (int i = 0; i < BOARD_ORDER; i++) {
            for (int j = 0; j < BOARD_ORDER; j++) {
                final int i2 = i;
                final int j2 = j;
                this.board[i][j] = new JTextField("");
                b.add(this.board[i][j]);
                this.board[i][j].addActionListener(e -> {
                    int v = Integer.parseInt(((JTextField) e.getSource()).getText());
                    if (!this.finished) {
                        if (humanMove(v, i2, j2)) {
                            ((JTextField) e.getSource()).setEnabled(false);
                            this.ns.getBoard().forEach(System.out::println);
                            System.out.println();
                            if (this.ns.checkVictory(this.currentPlayer)) {
                                this.finished = true;
                                for (JTextField[] jTextFields : this.board) {
                                    for (JTextField jTextField : jTextFields) {
                                        jTextField.setEnabled(false);
                                    }
                                }
                                String message = this.ns.getResult(this.currentPlayer);
                                System.out.println(message);
                                JOptionPane.showMessageDialog(null, message);
                            } else if (this.moves == Math.pow(BOARD_ORDER, 2)) {
                                String message = "even!";
                                System.out.println(message);
                                JOptionPane.showMessageDialog(null, message);
                            }
                            this.currentPlayer = this.ns.changeTurn(this.currentPlayer);
                        } else {
                            ((JTextField) e.getSource()).setText("");
                        }
                    }
                });
            }
        }
        JPanel s = new JPanel(new FlowLayout());
        s.add(this.exit);
        this.exit.addActionListener(e -> System.exit(0));
        this.frame.add(BorderLayout.CENTER, b);
        this.frame.add(BorderLayout.SOUTH, s);
        this.frame.setSize(this.FRAME_DIMENSION);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
        Player[] ps = Player.values();
        int returnValue = JOptionPane.CLOSED_OPTION;
        while (returnValue == JOptionPane.CLOSED_OPTION) {
            returnValue = JOptionPane.showOptionDialog(null, "Who makes the first move?", "Player's choice", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, ps, null);
        }
        this.currentPlayer = ps[returnValue];
    }

    private boolean humanMove(final int value, final int i, final int j) {
        if (this.ns.move(this.currentPlayer, value, i, j)) {
            this.board[i][j].setText(value + " - " + this.currentPlayer.toString());
            this.moves++;
            return true;
        }
        return false;
    }

    public static void main(final String[] args) {
        try {
            new NumberScrabbleApp(new NumberScrabbleImpl("src/model.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }

}
