% player(+OnBoard, -OnScreen): player for the board and their rendering
symbol(null, '_').
symbol(p1, 'X').
symbol(p2, 'O').

% cell_value(-InitialValue): defines the initial value of a board cell
cell_value({null,null}).

% board_order(-Order)
board_order(3).

% board_dimension(-Dimension)
board_dimension(BD) :- board_order(N), BD is N*N.

% winning_value(-Value): winning value
winning_value(W) :- board_order(O), board_dimension(D), W is (D+1)*O/2.

% create_board(-Board): creates an initially empty board
create_board(B) :- board_dimension(D), cell_value(CV), create_list(D, CV, B).
create_list(0, _, []) :- !.
create_list(N, X, [X|T]) :- N2 is N-1, create_list(N2, X, T).

% result(+OnBoard, -OnScreen): result of a game
result(even, "even!").
result(p1, "player 1 wins").
result(p2, "player 2 wins").

% other_player(?Player, ?OtherPlayer)
other_player(p1, p2).
other_player(p2, p1).

% exists(+Board, +Value): check if a value exists into a list
exists([{VAL, _}|BT], VAL) :- !.
exists([_|BT], VAL) :- exists(BT, VAL).

% insert_at(+Board, +Value, +Player, +Index, -Board): insert a value into a list at a specified index only if the list does not already contain that value
insert_at([CV|T], VAL, PL, 0, [{VAL, PL}|T]) :- cell_value(CV), !.
insert_at([H1|T1], VAL, PL, Idx, [H1|T2]) :- VAL>0, board_dimension(D), VAL<D+1, not(exists([H1|T1], VAL)), Idx2 is Idx-1, insert_at(T1, VAL, PL, Idx2, T2).

% finalpatt(+Pattern): gives a winning pattern for a 3x3 board
finalpatt([x,x,x,o,o,o,o,o,o]).
finalpatt([o,o,o,x,x,x,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x]).
finalpatt([x,o,o,x,o,o,x,o,o]).
finalpatt([o,x,o,o,x,o,o,x,o]).
finalpatt([o,o,x,o,o,x,o,o,x]).
finalpatt([x,o,o,o,x,o,o,o,x]).
finalpatt([o,o,x,o,x,o,x,o,o]).

% check_victory(+Board, +Player): check if a player wins
check_victory(B, PL) :- finalpatt(P), match(B, P, PL, C, S), board_order(BO), C=BO, winning_value(W), S=W, !.

% match(+Board, +Pattern, +Player, -Count, -Sum): checks if in the board, the player matches a winning pattern
match(_, [], _,0, 0).
match([CV|B], [x|P], PL, C, S) :- cell_value(CV), match(B,P,PL,C, S).
match([_|B], [o|P], PL, C, S) :- match(B, P, PL, C, S).
match([{VAL, PL}|B], [x|P], PL, C, S) :- match(B, P, PL, C2, S2), S is S2+VAL, C is C2+1.

