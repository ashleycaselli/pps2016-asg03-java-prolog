/**
  * Created by ashleycaselli on 29/04/17.
  */
object Predicate {

    private val board = "board"
    private val assert = "assert"
    private val retractall = "retractall"
    private val createBoard = "create_board"
    private val otherPlayer = "other_player"
    private val symbol = "symbol"
    private val insertAt = "insert_at"
    private val boardOrder = "board_order"
    private val checkVictory = "check_victory"
    private val result = "result"

    def board(s: String): String = composer(board, s)

    def assert(s: String): String = composer(assert, s)

    def retractall(s: String): String = composer(retractall, s)

    def createBoard(s: String): String = composer(createBoard, s)

    def otherPlayer(s: String): String = composer(otherPlayer, s)

    def symbol(s: String): String = composer(symbol, s)

    def insertAt(s: String): String = composer(insertAt, s)

    def boardOrder(s: String): String = composer(boardOrder, s)

    def checkVictory(s: String): String = composer(checkVictory, s)

    def result(s: String): String = composer(result, s)

    private def rpar(s: String): String = s"(${s})"

    private def composer(s1: String, s2: String): String = s"${s1}${rpar(s2)}"

}
