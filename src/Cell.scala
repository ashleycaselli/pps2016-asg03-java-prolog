/**
  * Created by ashleycaselli on 28/04/17.
  */
trait Cell {

    def player: Player

    def value: Int

    def player_=(player: Player): Unit

    def value_=(value: Int): Unit

}

case class CellImpl(override var player: Player, override var value: Int) extends Cell {

    override def toString = s"{$value,$player}"

}
