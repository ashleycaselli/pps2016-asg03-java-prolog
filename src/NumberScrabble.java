import java.util.List;
import java.util.Optional;

/**
 * Created by ashleycaselli on 28/04/17.
 */
public interface NumberScrabble {

    void createBoard();

    Optional<Integer> getBoardOrder();

    List<Optional<Cell>> getBoard();

    boolean move(Player player, int value, int i, int j);

    Player changeTurn(Player currentPlayer);

    boolean checkVictory(Player player);

    String getResult(Player player);

}
